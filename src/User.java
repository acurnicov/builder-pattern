public class User {
    private int ID;
    private String name;
    private String email;
    private String password;
    private int birthYear;

    private User(Builder builder) {
        this.ID = builder.ID;
        this.name = builder.name;
        this.email = builder.email;
        this.password = builder.password;
        this.birthYear = builder.birthYear;
    }

    @Override
    public String toString() {
        return "User: " + this.ID + ", " + this.name + ", " +
                this.email + ", " + this.password + ", " + this.birthYear;
    }

    public static class Builder {
        private int ID;
        private String name;
        private String email;
        private String password;
        private int birthYear;

        public Builder(String name, String email, String password) {
            this.name = name;
            this.email = email;
            this.password = password;
        }

        public Builder ID(int ID) {
            this.ID = ID;
            return this;
        }

        public Builder birthyear(int birthYear) {
            this.birthYear = birthYear;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    // getters
    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getBirthYear() {
        return birthYear;
    }
}
