public class Main {
    public static void main(String[] args) {
        User artur = new User.Builder("Artur", "artur@gmail.com", "pass123")
                .ID(123)
                .birthyear(1999)
                .build();

        System.out.println(artur);

        User petru = new User.Builder("Petru", "petru@gmail.com", "iampass303")
                .ID(214)
                .build();

        System.out.println(petru);
    }
}
